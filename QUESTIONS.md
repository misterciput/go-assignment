# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
at the first time I see this is a quiet good structure but after I code on that, it feel quiet complex to reuse the opened db connection. Also there is no space to add config on .ini files. So in this project I just hardcoded my db connection in .go file


 - **What you will improve from your solution ?**
add some configurable config in .ini files or .ctmpl files (if want to use consul). Also there should be a proper testing using dockertest, but I think I am not gonna do it for this time because the tight time. And also there still a db connection which always open, I think that's not good because it will block any other connection if opened too long.


 - **For you, what are the boundaries of a service inside a micro-service architecture ?**
 first thing is network issues. Based on my experience in Tokopedia, some of API from another team getting slow when someone hit it with request more than 600 rps (this API did not have any connection to any database). The issue is on the network pool which getting exhausted. Other thing become boundaries is the reusability of code. Sometimes we forget that we already have the same code which having same output but in other microservice modules and we just recreate that code in our microservice, it should be reusable but I think will be quiet hard

 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**
 For me, SQL will be more relevant to store relational and any transactional data which not so often to queries. 

 NoSQL will be good to do denormalization and indexing for search engine purpose like I did it in ElasticSearch, I denormalize several tables into one ElasticSearch index so it will be very easy to do filtering, analyzing, etc. 

 Key-Value is a good structure to do caching any data which you need the most. For example, user id to user name data, you can store it in key value pair so anytime you need it you will get it fast. Key-value usually come together with TTL so if the TTL is run out, the key-value will be deleted and some of memory will be free again.

 Document store is something ideal to store logging data, which we do not need it anytime. In Tokopedia, we use document store in Amazon S3 to save detailed logging such as click, impression, and any user activities. 