package http

import (
	"gitlab.com/zenport.io/go-assignment/engine"
	"gitlab.com/zenport.io/go-assignment/providers/database"
	"gitlab.com/zenport.io/go-assignment/domain"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
	"os"
	"encoding/json"
	"io/ioutil"
)

type HTTPAdapter struct{
	Router *mux.Router
	Engine engine.Engine
}

func (adapter *HTTPAdapter) Start() {
	// todo: start to listen
	// run on port 3000
	port := ":3000"
	fmt.Println("Fighter API Is Running...", port)
	http.ListenAndServe(port, handlers.LoggingHandler(os.Stdout, adapter.Router))
}

func (adapter *HTTPAdapter) Stop() {
	// todo: shutdown server
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {

	// todo: init your http server and routes
	r := mux.NewRouter()
	adapter := &HTTPAdapter{Router: r, Engine: e}

	r.HandleFunc("/knight", adapter.HandlerGetKnight).Methods("GET")
	r.HandleFunc("/knight/{id}", adapter.HandlerGetKnight).Methods("GET")
	r.HandleFunc("/knight", adapter.HandlerCreateKnight).Methods("POST")

	return adapter
}

func (adapter *HTTPAdapter) HandlerGetKnight(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	if id != "" {
		GetSingleKnight(w, params["id"], adapter.Engine)
	} else {
		GetAllKnight(w, adapter.Engine)
	}
}

func GetSingleKnight(w http.ResponseWriter, id string, e engine.Engine) {
	status := http.StatusOK

	knight, err := e.GetKnight(id)
	k := *knight
	if err != nil || knight == nil || k.GetID() == "" {
		status = http.StatusNotFound
	}

	writeResponse(w, knight, status)
}

func GetAllKnight(w http.ResponseWriter, e engine.Engine) {
	status := http.StatusOK

	knights := e.ListKnights()
	if len(knights) == 0 {
		status = http.StatusNotFound
	}

	writeResponse(w, knights, status)
}

func (adapter *HTTPAdapter) HandlerCreateKnight(w http.ResponseWriter, r *http.Request) {
	var knight database.Knight

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		writeResponse(w, err, http.StatusBadRequest)
		return
	}

	err = json.Unmarshal(body, &knight)
	if err != nil {
		writeResponse(w, err, http.StatusBadRequest)
		return
	}

	var domKnight domain.Knight
	domKnight = &knight

	err = adapter.Engine.Save(&domKnight)
	if err != nil {
		writeResponse(w, err, http.StatusBadRequest)
		return
	}

	writeResponse(w, "successfully inserted", http.StatusCreated)
}

func writeResponse(w http.ResponseWriter, data interface{}, status int){
	jsonData, _ := json.Marshal(data)
	w.Header().Add("Content-Type", "application/json")
    w.WriteHeader(status)
	w.Write(jsonData)
}
