package database

import ( 
	"gitlab.com/zenport.io/go-assignment/engine"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

type Provider struct {
	Connection  *sqlx.DB
	Error 		error
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	var knightRepo knightRepository

	if provider == nil {
		return &knightRepo
	}

	query := `
		SELECT *
		FROM knight
	`

	rows, err := provider.Connection.Queryx(query)
	if err != nil {
		log.Println(err)
		return &knightRepository{Error: err}
	}

	defer rows.Close()
	mapKnightsData := make(map[int64]Knight)

	for rows.Next() {
		var knight Knight
		if err := rows.StructScan(&knight); err != nil {
			rows.Close()
			return &knightRepository{Error: err}
		}

		mapKnightsData[knight.Id] = knight
	}

	knightRepo.KnightsData = mapKnightsData

	return &knightRepo
}

func (provider *Provider) Close() {
	if provider != nil {
		provider.Connection.Close()
	}
}

func NewProvider() *Provider {
	db, err := sqlx.Open("postgres", "dbname=fightknight user='mzulfs' password='' host=localhost port=5432 sslmode=disable")
	return &Provider{Connection: db, Error: err}
}
