package database

import (
	"gitlab.com/zenport.io/go-assignment/domain"
	"strconv"
	"fmt"
)

type knightRepository struct {
	Error   	error
	KnightsData map[int64]Knight
}

type Knight struct {
	Id 			int64 	`db:"id" json:"id"`
	Name 		string 	`db:"name" json:"name"`
	Strength 	float64 `db:"strength" json:"strength"`
	WeaponPower float64 `db:"weapon_power" json:"weapon_power"`
}

func (repository *knightRepository) Find(ID string) *domain.Knight {
	id, _ := strconv.ParseInt(ID, 10, 64)

	if id == 0 {
		return nil
	}

	var dmKnight domain.Knight
	value := repository.KnightsData[id]
	dmKnight = &value


	return &dmKnight
}

func (repository *knightRepository) FindAll() []*domain.Knight {
	arrKnight := make([]*domain.Knight, 0)

	for _, kn := range repository.KnightsData {
		knight := repository.Find(strconv.FormatInt(kn.Id, 10))

		arrKnight = append(arrKnight, knight)
	}

	return arrKnight
}

func (repository *knightRepository) Save(knight *domain.Knight) error {
	if knight == nil {
		return fmt.Errorf("No data to be inserted")
	}

	k := *knight

	provider := NewProvider()
	if provider.Error != nil {
		return provider.Error
	}

	tx := provider.Connection.MustBegin()

	insertSql := fmt.Sprintf(`
			INSERT INTO knight (name, strength, weapon_power)
			VALUES ('%s', %f, %f)
		`, k.GetName(), k.GetStrength(), k.GetWeaponPower())

	tx.MustExec(insertSql)
	err := tx.Commit()

	return err
}

func (knight *Knight) GetStrength() float64 {
	if knight == nil {
		return 0
	}

	return knight.Strength
}

func (knight *Knight) GetWeaponPower() float64 {
	if knight == nil {
		return 0
	}
	return knight.WeaponPower
}

func (knight *Knight) GetID() string {
	if knight == nil {
		return ""
	}

	return strconv.FormatInt(knight.Id, 10)
}

func (knight *Knight) GetPower() float64 {
	var power float64

	power = knight.GetStrength() + knight.GetWeaponPower()

	return power
}

func (knight *Knight) GetName() string {
	if knight == nil {
		return ""
	}
	return knight.Name
}