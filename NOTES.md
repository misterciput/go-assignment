# Notes

*Database setup, structure change, tests setup, etc.*

Database Setup

DBMS: PostgreSQL
Database Name: fightknight
User: mzulfs (configurable in provider.go function NewProvider)
Password: 
Port: 5432

CREATE TABLE knight (
    id integer NOT NULL,
    name text,
    strength double precision,
    weapon_power double precision
);