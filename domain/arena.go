package domain

type Arena struct{}

func (arena *Arena) Fight(fighter1 Fighter, fighter2 Fighter) Fighter {
	var fighterWin Fighter

	power1 := fighter1.GetPower()
	power2 := fighter2.GetPower()

	if power1 == power2 {
		return nil
	} else if power1 > power2 {
		fighterWin = fighter1
	} else {
		fighterWin = fighter2
	}

	return fighterWin
}
