package domain

type Fighter interface {
	GetID() string
	GetPower() float64
}

type Knight interface {
	GetStrength() float64
	GetWeaponPower() float64
	GetName() string
	Fighter
}
