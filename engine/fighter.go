package engine

import (
	"errors"
	"fmt"

	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	fighter := engine.knightRepository.Find(ID)
	if fighter == nil {
		return nil, errors.New(fmt.Sprintf("fighter with ID '%s' not found!", ID))
	}

	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	fighters := engine.knightRepository.FindAll()
	if len(fighters) == 0 {
		return nil
	}

	return fighters
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) domain.Fighter {
	var fighter domain.Fighter
	var knightWin domain.Knight

	knight1 := engine.knightRepository.Find(fighter1ID)
	knight2 := engine.knightRepository.Find(fighter2ID)

	k1 := *knight1
	k2 := *knight2

	power1 := k1.GetPower()
	power2 := k2.GetPower()

	if power1 == power2 {
		return nil
	} else if power1 > power2 {
		knightWin = k1
	} else {
		knightWin = k2
	}

	fighter = knightWin


	return fighter
}

func (engine *arenaEngine) Save(knight *domain.Knight) error {
	err := engine.knightRepository.Save(knight)
	if err != nil {
		return err
	}
	
	return nil
}
